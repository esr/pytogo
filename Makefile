# Makefile for the pytogo project
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERSION=$(shell sed <pytogo -n -e '/^version *= *\(.*\)/s//\1/p')

PREFIX=/usr
MANDIR=$(PREFIX)/share/man/man1
BINDIR=$(PREFIX)/bin

SOURCES = pytogo Makefile tests/
DOCS    = README.adoc NEWS.adoc BUGS.adoc pytogo.adoc pytogo.1
META    = COPYING control  # pytogo-logo.png
DISTRIB = $(SOURCES) $(DOCS) $(META) 

all: pytogo-$(VERSION).tar.gz

install: pytogo.1
	cp pytogo $(BINDIR)
	gzip <pytogo.1 >$(MANDIR)/pytogo.1.gz
	rm pytogo.1

pytogo-$(VERSION).tar.gz: $(DISTRIB)
	mkdir pytogo-$(VERSION)
	cp -r $(DISTRIB) pytogo-$(VERSION)
	tar -czf pytogo-$(VERSION).tar.gz pytogo-$(VERSION)
	rm -fr pytogo-$(VERSION)
	ls -l pytogo-$(VERSION).tar.gz

pytogo-$(VERSION).md5: pytogo-$(VERSION).tar.gz
	@md5sum pytogo-$(VERSION).tar.gz >pytogo-$(VERSION).md5

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

check:
	@$(MAKE) --quiet pylint
	@$(MAKE) -C tests --quiet

pylint:
	@pylint --score=n pytogo

dist: pytogo-$(VERSION).tar.gz

clean:
	rm -f pytogo.1 *.tar.gz *.md5 *old
	rm -f *.pyc *~ index.html $(DOCS:.adoc=.html)

NEWSVERSION=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

version:
	@echo "News version: $(NEWSVERSION), internal version: $(VERSION)"

release: pytogo-$(VERSION).tar.gz pytogo-$(VERSION).md5 pytogo.html
	@[ $(VERSION) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	@shipper version=$(VERSION) | sh -e -x

refresh: $(DOCS:.adoc=.html)
	@[ $(VERSION) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper -N -w version=$(VERSION) | sh -e -x

