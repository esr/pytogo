//# Test handling of blank line in mid-function.
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
func me() {
    fmt.Print(2)

    fmt.Print(3)
}

func you() {
    fmt.Print(5)
    fmt.Print(6)
}
