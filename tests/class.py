## Test class method translation
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
class NodeAction(object):
    __slots__ = ("revision", "path", "kind", "action",
                 "from_rev", "from_path", "content_hash",
                 "from_hash", "blob", "props", "propchange",
                 "from_set", "blobmark", "generated")
    # If these don't match the constants above, havoc will ensue
    ActionValues = ("add", "delete", "change", "replace")
    PathTypeValues = ("none", "file", "dir", "ILLEGAL-TYPE")
    def __init__(self, revision):
        # These are set during parsing
        self.revision = revision
        self.path = None
        self.kind = SD_NONE
        self.action = None
        self.from_rev = None
        self.from_path = None
        self.content_hash = None
        self.from_hash = None
        self.blob = None
        self.props = None
        self.propchange = False
        # These are set during the analysis phase
        self.from_set = None
        self.blobmark = None
        self.generated = False
    def __str__(self):
        # Prefer dict's repr() to OrderedDict's verbose one
        fmt = dict.__repr__ if isinstance(self.props, dict) else repr
        return "<NodeAction: r{rev} {action} {kind} '{path}'" \
                "{from_rev}{from_set}{generated}{props}>".format(
                rev = self.revision,
                action = "ILLEGAL-ACTION" if self.action is None else StreamParser.NodeAction.ActionValues[self.action],
                kind = StreamParser.NodeAction.PathTypeValues[self.kind or -1],
                path = self.path,
                from_rev = " from=%s~%s" % (self.from_rev, self.from_path)
                                if self.from_rev else "",
                from_set = " sources=%s" % self.from_set
                                if self.from_set else "",
                generated = " generated" if self.generated else "",
                props = " properties=%s" % fmt(self.props)
                                if self.props else "")
    __repr__ = __str__
