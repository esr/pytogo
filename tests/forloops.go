//# Test for-loop translation
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
        // Find all copy sources and compute the set of branches
        announce(DEBUG_EXTRACT, "Pass 1")
        nobranch = "--nobranch" in options
        copynodes = []
        for revision, record := range self.revisions {
            for _, node := range record.nodes {
                if node.from_path != nil {
                    copynodes = append(copynodes, node)
                    announce(DEBUG_EXTRACT, fmt.Sprintf("copynode at %s", node))
                }
                if node.action == SD_ADD && node.kind == SD_DIR && node.path+string(os.PathSeparator) !in self.branches && !nobranch {
                    for _, trial := range global_options["svn_branchify"] {
                        if "*" !in trial && trial == node.path {
                            self.branches[node.path+string(os.PathSeparator)] = nil
                        } else if trial.endswith(string(os.PathSeparator) + "*")
                                 && filepath.Dir(trial) == filepath.Dir(node.path)
                                 && node.path + string(os.PathSeparator) + "*" !in global_options["svn_branchify"] {
                            self.branches[node.path+string(os.PathSeparator)] = nil
                        } else if trial == "*" && node.path + string(os.PathSeparator) + "*" !in global_options["svn_branchify"] && node.path.count(string(os.PathSeparator)) < 1 {
                            self.branches[node.path+string(os.PathSeparator)] = nil
                        }
                    }
                    if node.path+string(os.PathSeparator) in self.branches && debug_enable(DEBUG_TOPOLOGY) {
                        announce(DEBUG_SHOUT, fmt.Sprintf("%s recognized as a branch", node.path)+string(os.PathSeparator))
                    }
                }
            }
            // Per-commit spinner disabled because this pass is fast
            //baton.twirl()
        }
