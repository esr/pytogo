//# Test conversion of string-in-list ifs
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
if string.Contains(bar, "foo")  {
    if string.Contains(quux, "b\"az\\\'")  {
        // if "fail" in comment {
        pass

    }
}
if "foo" in ("foo", "bar") {
    pass

}
if "foo" in (("foo", "bar") || ("baz", "quux")) {
    pass

}
if "foo" in ("foo",
             "bar") {
    pass

}
if fail in whatever {
    pass

}
if "fail" in [x for x in bar] {
    pass

}
if "fail" in [x for x in
              bar] {
    pass
}
