//# Simple test of block open-close translation
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
    // Return a list of tag name strings.
    func (self @@) get_taglist() {
        return self.tags
    }
    // Return an iterator yielding (reset name, revision) pairs.
    func (self @@) iter_resets() {
        return (item for item in self.refs.items() if "/tags/" !in item[0])
    }
    // Check the directory out to a specified revision, return a manifest.
    func (self @@) checkout(rev, filemap) {
        assert filemap != nil // pacify pylint
        assert rev != nil // pacify pylint
        return []
    }
    // Cleanup after checkout.
    func (self @@) cleanup(rev, issued) {
        assert rev && (issued != nil) // Pacify pylint
    }
    // Return the list of commit IDs of a commit's parents.
    func (self @@) get_parents(rev) {
        return self.parents[rev]
    }
    func (self @@) get_branch(rev) {
        return self.meta[rev]["branch"]
    }
    // Return a commit's change comment as a string.
    func (self @@) get_comment(rev) {
        assert rev != nil // pacify pylint
        return nil
    }
    func (self @@) get_committer(rev) {
        // Return the committer's ID/date as a string.
        return self.meta[rev]["ci"]
    }
    // Return the author's name and email address as a string.
    func (self @@) get_authors(rev) {
        return [self.meta[rev]["ai"]]
    }

