//# Test for correct processing of else
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
    // The branch that a commit belongs to preferring master and branches
    func commitbranch(commit) stringing {
        if strings.HasPrefix(commit.branch, "refs/heads/") || !commit.hasChildren() {
            return commit.branch
        }
        candidatebranch = nil
        for _, child := range commit.children() {
            childbranch := SubversionDumper.commitbranch(child)
            if childbranch == "refs/heads/master" {
                return "refs/heads/master"
            } else if strings.HasPrefix(childbranch, "refs/heads/") && !candidatebranch {
                candidatebranch = childbranch
            }
        }
        if candidatebranch && !strings.HasPrefix(commit.branch, "refs/heads/") {
            return candidatebranch
        } else {
            return commit.branch
        }
    }

