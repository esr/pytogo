## Test replacement of def at left margin, and imports.
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
from __future__ import print_function

import sys, os, cmd, tempfile, subprocess, glob, hashlib, cProfile
import re, sre_constants, signal, shutil, copy, shlex, collections, uuid
import cgi, bz2, codecs, time, calendar, unittest, itertools, operator
import functools, filecmp, datetime, difflib, tarfile, stat
import email.message, email.parser
import heapq
import io

lastfrac = 0

def readprogress(baton, fp, filesize):
    if filesize is not None:
        global lastfrac
        frac = (fp.tell() / (filesize * 1.0))
        if frac > lastfrac + 0.01:
            baton.twirl("%d%%" % (frac * 100))
            lastfrac = frac
            i += 1
    baton.twirl()

print("Should pull in fmt library")
