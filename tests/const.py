## Test translation of simple constants
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

A = 1
B  = 2
C =  3
D = 42 * 56
E = "foobar"
F = 3.14

