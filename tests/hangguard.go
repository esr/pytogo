//# Test that in case of a hanging conditional the opening bracket lands right.
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
        // This guard filters out the empty
        // nodes produced by format 7 dumps.
        if !(node.action == SD_CHANGE
                && node.props == nil
                && node.blob == nil
                && node.fromRev == nil) {
            nodes = append(nodes, node)
        }
        node = nil
