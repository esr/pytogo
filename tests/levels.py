## Test multiple levels of blockindent, try/finally.
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
    def _branch_color_items(self):
        # This is really cheating since fast-export could give us the
        # whole repo, but it's the only way I've found to get the correct
        # mapping of commits to branches, and we still want to test the
        # rest of the extractor logic independently, so here goes
        data = marks = None
        tfp, fname = tempfile.mkstemp()
        try:
            with popen_or_die("git fast-export --all --export-marks=%s" % fname) as fp:
                # We can't iterate line by line here because we need
                # to be sure that the entire fast-export process is
                # complete so the marks file is written and closed
                data = fp.read()
            with open(fname, "rb") as fp:
                marks = dict(polystr(line).strip().split() for line in fp)
        finally:
            os.close(tfp)
            os.remove(fname)
        if not (marks and data):
            raise Fatal("could not get branch information")
        branch = None
        for line in data.splitlines():
            fields = polystr(line).strip().split()
            if len(fields) != 2:
                continue
            elif fields[0] == r"commit":
                assert branch is None
                branch = fields[1]
            elif (fields[0] == "mark") and (branch is not None):
                h = marks[fields[1]]
                # This is a valid (commit hash, branch name) pair
                yield (h, branch)
                branch = None
            elif branch is not None:
                # The mark line for a commit should always be the next line after
                # the commit line, so this should never happen, but we put it in
                # just in case
                raise Fatal("could not parse branch information")

