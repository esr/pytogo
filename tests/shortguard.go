//# Test that conditionals with inline bodies transform correctly
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
    if x == 3 {
        holyhandgrenade()
    }
    if x != 5 {
        snuffit()
    }

