//# Test fix for pathological outdent
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
    for {
	line = sp.readline()
	if !line {
	    break
        } else if !strings.TrimSpace(line) {
	    continue
        } else if strings.HasPrefix(line, " # reposurgeon-read-options:") {
	    options = options.union(strings.Split(line, ":")[1].split())
        } else if strings.HasPrefix(line, "UUID:") {
	    sp.repo.uuid = sdBody(line)
        } else if strings.HasPrefix(line, "Revision-number: ") {
	    // Begin Revision processing
	    announce(debugSVNPARSE, fmt.Sprintf("revision parsing, line %d: begins", sp.importLine))
        }
    }

