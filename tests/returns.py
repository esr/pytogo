## Test return-type detection
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

def predicate(e):
    "An example docstring."
    return True

def predicate2(e):
    "Another example docstring."
    return 0

def pep484(a: int, b: str) -> str:
    print("I see you!")
    return b % a

# Python program to illustrate
# *args with first extra argument
def myFun(arg1, *argv):
    print ("First argument :", arg1)
    for arg in argv:
        print("Next argument through *argv :", arg)
    
