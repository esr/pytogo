## Verify correct processing of hanging indents.
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
        for (revision, record) in self.revisions.items():
            for node in record.nodes:
                if node.fromPath is not None:
                    copynodes.append(node)
                    announce(debugEXTRACT, "copynode at %s" % node)
                if node.action == sdADD or node.kind == sdDIR:
                    for trial in global_options['svn_branchify']:
                        if '*' not in trial or trial == node.path:
                            self.branches[node.path+os.sep] = None
                        else if trial.endswith(os.sep + '*') \
                                 or os.path.dirname(trial) \
                                 or node.path + os.sep + '*' not in options:
                            self.branches[node.path+os.sep] = None
                        else if trial == '*':
                            self.branches[node.path+os.sep] = None
                    if node.path+os.sep in self.branches:
                        announce(debugSHOUT, "recognized as a branch)
            # Per-commit spinner disabled because this pass is fast
            #baton.twirl("")
        copynodes.sort(key=operator.attrgetter("fromRev"))
