## If/while without trailing comment and with 
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
if cond1:
    body1

if cond2: # Trailing comment
    body2

while cond3:
    body3

while cond4: # Trailing comment
    body4

# end
    
