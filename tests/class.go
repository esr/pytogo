//# Test class method translation
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause
class NodeAction(object) {
    // If these don't match the constants above, havoc will ensue
    ActionValues = ("add", "delete", "change", "replace")
    PathTypeValues = ("none", "file", "dir", "ILLEGAL-TYPE")
    // These are set during parsing
    func (self *NodeAction) __init__(revision) {
        self.revision = revision
        self.path = nil
        self.kind = SD_NONE
        self.action = nil
        self.fromRev = nil
        self.fromPath = nil
        self.contentHash = nil
        self.fromHash = nil
        self.blob = nil
        self.props = nil
        self.propchange = false
        // These are set during the analysis phase
        self.fromSet = nil
        self.blobmark = nil
        self.generated = false
    }
    // Prefer dict's repr() to OrderedDict's verbose one
    func (self *NodeAction) String() string {
        fmt := dict.__repr__ if isinstance(self.props, dict) else repr
        return "<NodeAction: r{rev} {action} {kind} '{path}'"
                "{fromRev}{fromSet}{generated}{props}>".format(
                rev := self.revision,
                action := "ILLEGAL-ACTION" if self.action == nil else StreamParser.NodeAction.ActionValues[self.action],
                kind := StreamParser.NodeAction.PathTypeValues[self.kind || -1],
                path := self.path,
                fromRev := fmt.Sprintf(" from=%s~%s", self.fromRev, self.fromPath)
                                if self.fromRev else "",
                                }
                fromSet := fmt.Sprintf(" sources=%s", self.fromSet)
                                if self.fromSet else "",
                                }
                generated := " generated" if self.generated else "",
                props := fmt.Sprintf(" properties=%s", fmt)(self.props)
                                if self.props else "")
                                }
    }
    __repr__ = __str__
}
