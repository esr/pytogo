//# Test translation of simple constants
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause

const A = 1
const B  = 2
const C =  3
const D = 42 * 56
const E = "foobar"
const F = 3.14

