## Test that in case of a hanging conditional the opening bracket lands right.
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
        # This guard filters out the empty
        # nodes produced by format 7 dumps.
        if not (node.action == SD_CHANGE
                and node.props is None
                and node.blob is None
                and node.from_rev is None):
            nodes.append(node)
        node = None
