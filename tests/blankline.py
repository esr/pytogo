## Test handling of blank line in mid-function.
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
def me():
    print(2)

    print(3)

def you():
    print(5)
    print(6)
