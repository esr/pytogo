## Test for-loop translation
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
        # Find all copy sources and compute the set of branches
        announce(DEBUG_EXTRACT, "Pass 1")
        nobranch = '--nobranch' in options
        copynodes = []
        for (revision, record) in self.revisions.items():
            for node in record.nodes:
                if node.from_path is not None:
                    copynodes.append(node)
                    announce(DEBUG_EXTRACT, "copynode at %s" % node)
                if node.action == SD_ADD and node.kind == SD_DIR and node.path+os.sep not in self.branches and not nobranch:
                    for trial in global_options['svn_branchify']:
                        if '*' not in trial and trial == node.path:
                            self.branches[node.path+os.sep] = None
                        elif trial.endswith(os.sep + '*') \
                                 and os.path.dirname(trial) == os.path.dirname(node.path) \
                                 and node.path + os.sep + '*' not in global_options['svn_branchify']:
                            self.branches[node.path+os.sep] = None
                        elif trial == '*' and node.path + os.sep + '*' not in global_options['svn_branchify'] and node.path.count(os.sep) < 1:
                            self.branches[node.path+os.sep] = None
                    if node.path+os.sep in self.branches and debug_enable(DEBUG_TOPOLOGY):
                        announce(DEBUG_SHOUT, "%s recognized as a branch" % node.path+os.sep)
            # Per-commit spinner disabled because this pass is fast
            #baton.twirl()
